function colocar_terminal(num, v){
    var saida_nota_add = document.createElement('p');
    saida_nota_add.textContent = "A nota " + num + " é " + parseFloat(v).toFixed(2) + ".";
    var temp = document.getElementsByClassName("terminal");
    temp[0].appendChild(saida_nota_add);
}

function saida_resultado(media_saida) {
    output = "A média aritmética das notas é " + media_saida.toFixed(2);
    document.getElementById("saida_resultado").innerHTML = output
}

function somar_valor() {
    let valor = document.querySelector("#entrada_escrita_nota").value
    if (valor == "") {
        alert("Por favor, insira uma nota.");
    } else if (isNaN(valor)) {
        alert("A nota digitada é inválida, por favor, insira uma nota válida.");
    } else if (valor < 0 || valor > 10) {
        alert("A nota digitada é inválida, por favor, insira uma nota válida.");
    } else {
        numero_de_notas_add++;
        soma_total += parseFloat(valor);
        colocar_terminal(numero_de_notas_add, valor)
        valor = ""
    }
}

function calculo_media() {
    let media_saida = 0
    if (numero_de_notas_add != 0) {
        media_saida = soma_total / numero_de_notas_add
    }
    saida_resultado(media_saida)
}


let numero_de_notas_add = 0
let soma_total = 0

// Parte da entrada de valores para fazer a média
var valor_entrada = document.querySelector("#botao_entrada_valor")
valor_entrada.addEventListener("click", somar_valor)

// Parte da saida da média
var saida = document.querySelector("#botao_calcular_saida")
saida.addEventListener("click", calculo_media)
